FROM python:3.11-alpine as base
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN /usr/local/bin/python -m pip install --upgrade pip

FROM base AS builder
RUN mkdir -p /build
WORKDIR /build
COPY /api/requirements.txt /build/requirements.txt
RUN pip install --prefix=/build -r /build/requirements.txt

FROM base
COPY --from=builder /build /usr/local
WORKDIR /code
COPY /api /code

EXPOSE 8000
CMD hypercorn app:app --bind 0.0.0.0:8000 --reload
