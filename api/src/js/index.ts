const hide_button = document.getElementById('hide_button') as HTMLButtonElement;

let card_hidden: boolean = false;

const swapIcon = (
  current_icon_id: string,
  classes: string,
  next_icon_id: string
) => {
  const current_icon_element = document.getElementById(
    current_icon_id
  ) as HTMLElement;

  let next_icon_element = document.createElement('i');
  next_icon_element.className = classes;
  next_icon_element.setAttribute('id', next_icon_id);

  hide_button.removeChild(current_icon_element);
  hide_button.appendChild(next_icon_element);
};

const hideCard = () => {
  card_hidden = !card_hidden;

  document.getElementById('eye_slashed_icon') !== null
    ? swapIcon('eye_slashed_icon', 'fa-solid fa-eye', 'eye_icon')
    : swapIcon('eye_icon', 'fa-solid fa-eye-slash', 'eye_slashed_icon');

  const card = document.getElementById('card') as HTMLDivElement;

  card_hidden
    ? (card.className = 'card hidden')
    : (card.className = 'card shown');
};

hide_button.addEventListener('click', hideCard);
