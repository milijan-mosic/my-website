from quart import Quart, render_template, send_from_directory

app = Quart(__name__, static_folder='static', static_url_path='')


@app.route('/', methods=['GET'])
async def index():
    return await render_template('pages/index.html'), 200


@app.route('/<path:path>', methods=['GET'])
async def serve_global_file(path):
    return await send_from_directory(path), 200


@app.errorhandler(404)
async def page_not_found(error):
    return await render_template('pages/404.html'), 404


if __name__ == '__main__':
    app.run()
