- Get Let's Encrypt certificate
- Write Docker files for production!
- Do CI/CD
- Submit sitemap

- Reduce FontAwesome scripts
- Ensure cood CSP in HTTP header
- Make "passive" listeners to the 'dat gui' script
- SERVE STATIC FILES VIA CACHE!

- Modernize fluid renderer (to JS first, improve it and then transfer it to TS)
- Write better config file for TypeScript
- Minify JS
