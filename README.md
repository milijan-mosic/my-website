# My Website

## Get started

Clone the project

```sh
git clone git@gitlab.com:milijan-mosic/my-website.git
cd my-website/
```

### First terminal

Run TailwindCSS compiler

```sh
cd my-website/api/
npm install
npx tailwindcss -i ./src/css/globals.css -o ./static/css/globals.min.css --watch --minify
```

### Second terminal

Run TypeScript to JavaScript compiler

```sh
cd my-website/api/
npm run watch
```

### Third terminal

1. Copy this code:

```sh
127.0.0.1   app.my-website.local.com
```

2. Paste it:

```sh
sudo vim /etc/hosts
```

3. Run app:

```sh
docker-compose up --build --remove-orphans
```

4. Access the website via [https://app.my-website.local.com/](https://app.my-website.local.com/)

### References (WebGL Fluid Simulation)

[https://paveldogreat.github.io/WebGL-Fluid-Simulation/](https://paveldogreat.github.io/WebGL-Fluid-Simulation/) <3
